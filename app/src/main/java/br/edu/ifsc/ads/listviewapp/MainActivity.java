package br.edu.ifsc.ads.listviewapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = findViewById(R.id.listV);

        String[] arrStr = new String[]{"Nome 1","Nome 2", "Nome 3","Nome 4", "Nome 5","Nome 6"};

        ArrayAdapter<String> adpt = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                arrStr
                );

        lv.setAdapter(adpt);
    }
}
